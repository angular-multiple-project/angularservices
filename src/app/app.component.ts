import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent{
  public title = 'Angular-Services';
  public home = 'Welcome to Home PAge';
  public displayonScreen = "";
  public user = "";
  public message = "";
  public name = "Pankaj Kumar";

  clickButton(event){
    this.displayonScreen = "Working event Binding";
    //console.log(event);
  }
  clickButton1(event){
    this.displayonScreen = event;
  }

  LoginPage(value){
    this.user = "This is user id = " + value;
  }

}
 